from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from .forms import ProfileForm
from .models import Profile
from django.urls import reverse, resolve

# Create your views here.
def show_users(request):
    users = Profile.objects.all()
    context = {'users': users}
    return render(request, template_name='users.html', context=context)

def create(request):
    if request.method == "POST":
        form = ProfileForm(request.POST)
        if form.is_valid():
            profile = form.save(commit=False)
            profile.save()
            # Redirect to homepage
            return redirect('/')
    # for the GET request
    else:
        form = ProfileForm()
    return render(request, 'form.html', {'form': form, 'message': 'Create User'})

def update(request, pk):
    profile = get_object_or_404(Profile, pk=pk)
    if request.method == "POST":
        form = ProfileForm(request.POST, instance=profile)
        if form.is_valid():
            profile = form.save(commit=False)
            profile.save()
            return redirect('/', pk=profile.pk)
    else:
        form = ProfileForm(instance=profile)
    return render(request, 'form.html', {'form': form, 'message': 'Edit User Profile'})

def delete(request, pk):
    user_to_delete = Profile.objects.get(id=pk)
    user_to_delete.delete()
    return redirect('/', message='User Deleted')


