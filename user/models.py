from django.db import models

# Create your models here.
class Profile(models.Model):
    firstname = models.CharField(max_length=120)
    middlename = models.CharField(max_length=120, blank=True, null=True)
    lastname = models.CharField(max_length=120)
    extname = models.CharField(max_length=30, blank=True, null=True)
    birthdate = models.DateField()

    def __str__(self):
        return f'User ID: {self.id} | Name: {self.firstname} {self.lastname}'

